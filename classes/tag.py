# import needed modules
import sqlite3
from configQuotes import Config

# create a config object so we can access it's properties
config = Config()
# ---------------------------------------------

 
class Tag:

    # this is a contructor class
    # it holds properties of the object that can be 
    # referred to throughout via "self"
    def __init__(self):
        # put properties here

        # path is relative to root of app (app.py)
        # it comes from the config.py class in the root folder
        self.dbPath = config.dbName




    def retrieveAllTags(self):

        with sqlite3.connect(self.dbPath) as con:

            con.row_factory = sqlite3.Row
            cur = con.cursor()
            cur.execute("SELECT * FROM tags")
            result = cur.fetchall()
            cur.close()

            if result:

                tagList = list()

                for key,value in result:
                    tagList.append(value)

                return tagList
            
            else:
                return False 






    def addTags(self,tagString):

        tagListRaw = tagString.strip().split(',')
        tagList = []

        for roughTag in tagListRaw:

            roughTag = roughTag.replace(" ","")
            tagList.append(roughTag.strip())

        # print(tagList)

        with sqlite3.connect(self.dbPath) as con:

            for potentialTag in tagList:

                con.row_factory = sqlite3.Row
                cur = con.cursor()
                cur.execute("SELECT * FROM tags WHERE tag = ?",[potentialTag])
                result = cur.fetchone()
                cur.close()

                if result is None:

                    print("Adding to tags table:", potentialTag)
                    cur = con.cursor()
                    success = cur.execute("INSERT INTO tags (tag) VALUES (?)", [potentialTag])
                    con.commit()
                    cur.close()

                    if success == False:
                        app.logger.info("Could not insert new tag:",potentialTag)
                        return False
        
        return tagList





    def assignTags(self,quoteId,tagList):

        returnValue = True 

        with sqlite3.connect(self.dbPath) as con:

            for tag in tagList:

                con.row_factory = sqlite3.Row
                cur = con.cursor()
                cur.execute("SELECT * FROM tags WHERE tag = ?",[tag])
                result = cur.fetchone()
                cur.close()

                if result:
                    tagId = result['id']

                    cur = con.cursor()
                    success = cur.execute("INSERT INTO quotes_tags (id_quotes,id_tags) VALUES (?,?)",[quoteId,tagId])
                    con.commit()
                    cur.close()

                    if success == False:
                        returnValue = "Had an issue assigning quoteId: "+quoteId+" to tagId: "+tagId
                        app.logger.info(returnValue)
                        print(returnValue)

        return returnValue

